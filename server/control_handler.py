from SocketServer import BaseRequestHandler
from my_threading import Event, sleep
from blinken import *
from socket import error

__all__ = ['ControlHandler']

MAX_FRAME_SIZE = 4096

def parse_command(cmd):
    cmd = cmd.lower().strip()
    command, _, args = cmd.partition(' ')
    return (command, args.split(' ') if args else [])


class ControlHandler(BaseRequestHandler, object):
    def authenticate(self):
        #TODO
        pass

    def handle(self):
        self.authenticate()

        self.file = self.request.makefile()

        command, args = parse_command(self.file.readline())

        try:
            print command, args
            getattr(self, "do_" + command)(*args)
        except:
            import sys
            type, val, _ = sys.exc_info()
            msg = '!' + type.__name__ + ': ' + str(val) + '\r\n'
            print msg
            self.file.write(msg)

    def finish(self):
        self.file.close()
        self.request.close()

    def do_next(self):
        """Skip the current playing movie/projector."""
        self.server.force_next = True

    def do_display(self, x, y):
        """Show what the server currently shows for dimensions <x> * <y>."""
        barrier = Event()
        def callback(frame):
            try:
                self.request.send(build_frame(frame, int(x), int(y)))
            except error:
                barrier.set()
        self.server.add_frame_callback(callback)
        barrier.wait()

    def do_add_movie(self, name):
        """Add subsequent movie under the name <name>."""
        self.server.movies[name] = parse_movie(self.file.read())

    def do_add_idle(self, name):
        """Add movie <name> to the set of idles."""
        if name in self.server.movies and not name in self.server.idles:
            self.server.idles.append(name)

    def do_list_movies(self):
        """List movies saved on the server."""
        self.file.write('\n'.join(self.server.movies.iterkeys()))

    def do_list_idles(self):
        """List set of idles."""
        self.file.write('\n'.join(self.server.idles))

    def do_list_queue(self):
        """Show current playlist."""
        self.file.write('\n'.join(self.server.queue))

    def do_commands(self):
        """List available commands."""
        from inspect import getargspec
        for cmd in dir(self):
            obj = getattr(self, cmd)
            if cmd.startswith('do_') and hasattr(obj, '__call__'):
                msg = cmd[len('do_'):]
                for arg in getargspec(obj)[0][1:]:
                    msg += ' <%s>' % arg
                if obj.__doc__:
                    msg += ":\t" + obj.__doc__[:obj.__doc__.find('\n')]
                self.file.write(msg + '\r\n')

    def do_enqueue(self, name):
        """Enqueue movie <name>."""
        self.server.enqueue_movie(name)

    def do_remove_movie(self, name):
        """Remove movie <name>."""
        if name != 'dummy':
            self.server.movies.pop(name, '')
            if name in self.server.idles:
                self.server.idles.remove(name)

    def do_control(self):
        """Take control over the server."""
        barrier = Event()

        def control_projector():
            while True:
                try:
                    frame = self.request.recv(MAX_FRAME_SIZE)
                    yield parse_frame(frame)
                except error:
                    barrier.set()
                    break
        
        self.server.queue.insert(0, control_projector())
        self.server.force_next = True
        barrier.wait()


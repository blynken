
__all__ = ['Thread', 'schedule', 'sleep', 'ThreadingMixIn', 'Lock']

import time

try:
    import stackless
    import stackless_socket
    import socket
    stackless_socket.install()

    Thread = stackless.tasklet
    schedule = stackless.schedule

    from heapq import heappush

    sleepingTasklets = []

    class Lock(object):
        __enter__ = lambda *args: None
        __exit__ = lambda *args: None

    class Event(object):
        val = False
        channel = stackless.channel()

        def set(self):
            val = True
            self.channel.send(None)
        def clear(self):
            val = False
        def wait(self):
            self.val or self.channel.receive()

    def sleep(seconds):
        channel = stackless.channel()
        endTime = time.time() + seconds
        heappush(sleepingTasklets, (endTime, channel))
        # Block until we get sent an awakening notification.
        channel.receive()

    class ThreadingMixIn:
        def handle_request(self):
            try:
                request, client_address = self.get_request()
            except socket.error:
                return
            stackless.tasklet(self.handle_request_tasklet)(request, client_address)
            stackless.schedule()

        def handle_request_tasklet(self, request, client_address):
            if self.verify_request(request, client_address):
                try:
                    self.process_request(request, client_address)
                except:
                    self.handle_error(request, client_address)
                    self.close_request(request)

    def ManageSleepingTasklets():
        while True:
            while len(sleepingTasklets):
                endTime = sleepingTasklets[0][0]
                if endTime > time.time():
                    break
                channel = sleepingTasklets[0][1]
                sleepingTasklets.pop(0)
                # It does not matter what we send as it is not used.
                channel.send(None)
            stackless.schedule()

    stackless.tasklet(ManageSleepingTasklets)()

except ImportError:
    from threading import Thread as PyThread
    from threading import Lock, Event
    from SocketServer import ThreadingMixIn

    class Thread(PyThread):
        def __init__(self, func):
            super(Thread, self).__init__(target=func)

        def __call__(self):
            super(Thread, self).start()

    def schedule():
        pass

    sleep = time.sleep

    

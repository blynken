
def parse_movie(data):
    """Parse a string of data as a blinkenlights movie into a list."""
    res = []

    for frame in data.strip().split('@')[1:]:
        time, _, frame = frame.partition('\n')
        res.append((int(time), parse_frame(frame)))

    return res

def parse_frame(frame):
    """Parse a blinkenframe into a frozenset of 2-tuples defining on-pixels."""
    lines = frame.split('\n')
    parsed_frame = []
    y = 0
    for line in lines:
        if line.startswith('#') or not line:
            continue
        for x, val in zip(xrange(len(line)), line):
            if val == '1':
                parsed_frame.append((x, y))
        y += 1
    return frozenset(parsed_frame)
    
def build_frame(frame, x, y):
    """Build a blinkenframe from a set that provides "in" for 2-tuples."""
    res = ""
    for y_pos in xrange(y):
        for x_pos in xrange(x):
            res += '1' if (x_pos, y_pos) in frame else '0'
        res += '\n'
    return res


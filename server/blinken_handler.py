from __future__ import with_statement
from SocketServer import BaseRequestHandler

M_OKAY = 'okay\r\n'
M_NOPE = 'nope\r\n'

class BlinkenHandler(BaseRequestHandler):
    """RequestHandler controlling the nodes (pixels)"""

    def handle(self):
        self.server.close_request = lambda request : None

        send = self.request.send
        recv = self.request.recv
        data = recv(4096)
        coords = tuple(int(i) for i in data.split(':'))
        if len(coords) == 2:
            with self.server.lock:
                self.server.connections[coords] = self.request
            send(M_OKAY)
        else:
            send(M_NOPE)
            self.request.close()

    def finish(self):
        pass


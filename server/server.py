#!/usr/bin/env python
from __future__ import with_statement
from blinken_handler import BlinkenHandler
from control_handler import ControlHandler
from my_threading import *
from SocketServer import TCPServer
from socket import error

__all__ = ['BlinkenServer']

def dummy_projector():
    while True:
        sleep(0.1)
        yield []

def movie_projector(movie):
    for time, frame in movie:
        yield frame
        sleep(time * 0.001)

def wipe_effect(last_frame, next_frame, x_size, y_size, duration = 0.5):
    for x in xrange(x_size):
        current_frame = set([t for t in last_frame if t[0] > x] \
                          + [(x, y) for y in xrange(y_size)] \
                          + [t for t in next_frame if t[0] < x])
        yield current_frame
        sleep(duration * float(x) / x_size)


class Server(ThreadingMixIn, TCPServer):
    allow_reuse_address = True

class BlinkenServer(Server):
    blinken_port = 10000
    control_port = 10001
    force_next = False
    _is_idling = True
    queue = []
    _frame_callbacks = []
    idles = []
    movies = {'dummy': dummy_projector()}
    effects = [wipe_effect]

    def __init__(self):
        self._blinken = Server(('', self.blinken_port), BlinkenHandler)
        self._blinken.connections = {}
        self._blinken.lock = Lock()

        Server.__init__(self, ('', self.control_port), ControlHandler)

        Thread(self._serve_nodes)()
        Thread(self._blinken.serve_forever)()
        Thread(self.serve_forever)()

    def enqueue_movie(self, name):
        self.queue.append(movie_projector(self.movies[name]))

    def add_frame_callback(self, callback):
        self._frame_callbacks.append(callback)

    def clear_frame_callbacks(self):
        self._frame_callbacks.clear()

    def _projectors(self):
        import random
        while True:
            if not self.queue:
                self._is_idling = True
                if self.idles:
                    yield movie_projector(self.movies[random.choice(self.idles)])
                else:
                    yield dummy_projector()
            else:
                self._is_idling = False
                yield self.queue.pop(0)

    def _send_frame_to_nodes(self, frame):
        self._last_frame = frame
        to_clean = []

        for cb in self._frame_callbacks:
            try:
                cb(frame)
            except:
                _frame_callbacks.remove(cb)

        with self._blinken.lock:
            val = { True: 'on', False: 'off' }
            for pixel, sock in self._blinken.connections.iteritems():
                try:
                    sock.send(val[pixel in frame] + "\r\n")
                except error:
                    to_clean.append(pixel)
            for i in to_clean:
                del self._blinken.connections[i]

        schedule()


    def _serve_nodes(self):
        for projector in self._projectors():
            for frame in projector:
                if self._changed:
                    self._changed = False
                    for frame in wipe_effect(self._last_frame, frame, 8, 6):
                        self._send_frame(frame)
                
                self._send_frame(frame)
                
                if self.force_next or self.queue and self._is_idling:
                    self.force_next = False
                    break
                        _
            self._changed = True


if __name__ == '__main__':
    from sys import argv
    from optparse import OptionParser
    import cPickle as pickle

    opts = OptionParser()

    opts.add_option('-f', '--file', dest="filename")

    server = BlinkenServer()
    try:
        import stackless
        stackless.run()
    except ImportError:
        pass

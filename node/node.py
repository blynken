#!/usr/bin/env python
import asyncore, socket

class Node(asyncore.dispatcher):
    def __init__(self, host, coords, process=lambda self, val: None):
        self.coords = coords
        self.process = process
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setblocking(1)
        self.connect(host)
        self.socket.setblocking(0)
    
    def handle_connect(self):
        self.send("%i:%i\r\n" % self.coords)
        data = self.recv(4096)
        if data != "okay\r\n":
            raise RuntimeError
        self.writable = lambda : False

    def handle_read(self):
        data = self.recv(4096)
        if data == 'on\r\n':
            self.process(True)
        elif data == 'off\r\n':
            self.process(False)

    def handle_close(self):
        pass


if __name__ == '__main__':
    from sys import argv

    host = ('', 10000)
    nodes = []

    try:
        import parallel
        from functools import partial

        class MyParallel(parallel.Parallel):
            _data = 0

            def setData(self, data):
                self._data = data
                super(MyParallel, self).setData(data)

            def setPin(self, pin, val):
                self.setData((bool(val) << pin) | self._data)

        port = MyParallel()
        processor = lambda n: partial(port.setPin, n)
        
    except ImportError, OSError:
        import sys
        print sys.exc_info()[1]
        def processor(n):
            def process(val):
                print n, ': ', val
            return process

    if len(argv) > 1:
        port = MyParallel(self)
        for i in argv[1:]:
            pin, _, xy = i.partition('=')
            x, _, y = xy.partition(':')
            nodes.append(Node(host, (int(x), int(y)), process=processor(pin)))
    else:
        for x in range(8):
            for y in range(6):
                nodes.append(Node(host, (x, y), process=processor(x + 8 * y)))

    asyncore.loop()

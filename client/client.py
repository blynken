#!/usr/bin/env python

from context import Context

class CliContext(Context):
    def add_movie(self, sock, name, data=None):
        if not data:
            from sys import stdin
            data = stdin
        elif type(data) == str:
            data = file(data, 'r')
        super(CliContext, self).add_movie(sock, name, data)

class ControllerContext(Context):
    def control(self, sock, controller, x_size, y_size):
        from blinken import build_frame
        for frame in controller(x_size, y_size):
            sock.send(build_frame(frame, x_size, y_size))

if __name__ == '__main__':
    from optparse import OptionParser

    parser = OptionParser()

    parser.add_option("-s", "--host", dest="host")
    parser.add_option("-p", "--port", type=int, dest="port", default=10001)
    parser.add_option("-c", "--command", dest="command", default="commands")
    parser.add_option("--spectrum", action="store_true", dest="spectrum")
    parser.add_option("--life", action="store_true", dest="life")

    options, args = parser.parse_args()

    if not options.spectrum and not options.life:
        ctx = CliContext((options.host, options.port))
        getattr(ctx, options.command)()
    else:
        if options.spectrum:
            from controls.spectrum import spectrum_controller as cont
        else:
            from controls.life import life_controller as cont
        from functools import partial

        ctx = ControllerContext((options.host, options.port))
        ctx.control(cont, 8, 6)


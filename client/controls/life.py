
__all__ = ['life_controller']

from functools import partial

get_neighbors = \
        lambda index, x, y: index(x+1, y+1) + index(x, y+1) + index(x-1, y+1) \
                          + index(x+1, y) + index(x-1, y) \
                          + index(x+1, y-1) + index(x, y-1) + index(x-1, y-1)


def life_controller(x_size, y_size, rule=([2,3], [3]), p=0.5):
    field, next = set(), set()

    rule = rule[1], rule[0]

    from random import random
    from time import sleep

    for x in xrange(x_size):
        for y in xrange(y_size):
            if random() < p:
                field.add((x, y))

    index = lambda x, y: ((x % x_size), (y % y_size)) in field

    while True:
        for x in xrange(x_size):
            for y in xrange(y_size):
                if get_neighbors(index, x, y) in rule[index(x, y)]:
                    next.add((x, y))
        field, next = next, set()
        yield field
        sleep(0.2)


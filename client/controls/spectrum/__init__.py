from ctypes import CDLL, ARRAY, c_float
from time import sleep
from math import log

def spectrum_controller(x_size, y_size):
    spectrumsize = 256
    spectrum = ARRAY(c_float, spectrumsize)()
    delta = int(float(spectrumsize) / x_size)
    fmod = CDLL('libspectrum.dylib')
    fmod.init()

    while True:
        sleep(0.01)
        fmod.get_spectrum(spectrum, spectrumsize)
        my_spectrum = [sum(spectrum[x:x+delta]) for x in xrange(0, spectrumsize, delta)]
        print my_spectrum
        res = []
        for x in xrange(x_size):
            res += [(x, y) for y in xrange(y_size) if y / y_size < spectrum[x]]
        yield frozenset(res)

    fmod.deinit()


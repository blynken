#include <fmodex/fmod.h>
#include <fmodex/fmod_errors.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}

#define OUTPUTRATE          44100
#define SPECTRUMSIZE        8192
#define SPECTRUMRANGE       ((float)OUTPUTRATE / 2.0f)      /* 0 to nyquist */

#define BINSIZE      (SPECTRUMRANGE / (float)SPECTRUMSIZE)

static FMOD_SYSTEM* fmod_system = 0;
static FMOD_SOUND* sound = 0;
static FMOD_CHANNEL* channel = 0;

void init()
{
    FMOD_RESULT            result;
    FMOD_CREATESOUNDEXINFO exinfo;

    result = FMOD_System_Create(&fmod_system);
    ERRCHECK(result);

    result = FMOD_System_SetOutput(fmod_system, FMOD_OUTPUTTYPE_COREAUDIO);
    ERRCHECK(result);
    
//    result = FMOD_System_SetRecordDriver(fmod_system, 1);
    ERRCHECK(result);
 
    result = FMOD_System_SetSoftwareFormat(fmod_system, OUTPUTRATE, FMOD_SOUND_FORMAT_PCM16, 2, 0, FMOD_DSP_RESAMPLER_LINEAR);
    ERRCHECK(result);

    result = FMOD_System_Init(fmod_system, 32, FMOD_INIT_NORMAL, NULL);
    ERRCHECK(result);

    int outputfreq;
    FMOD_System_GetSoftwareFormat(fmod_system, &outputfreq, 0, 0, 0, 0, 0);
    ERRCHECK(result);

    /*
        Create a sound to record to.
    */
    memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));

    exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo.numchannels      = 2;
    exinfo.defaultfrequency = OUTPUTRATE;
    exinfo.format = FMOD_SOUND_FORMAT_PCMFLOAT;
    exinfo.length = exinfo.defaultfrequency * sizeof(float) * exinfo.numchannels * 5;

    result = FMOD_System_CreateSound(fmod_system, 0, FMOD_2D | FMOD_SOFTWARE | FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &sound);
    ERRCHECK(result);

    result = FMOD_System_RecordStart(fmod_system, sound, 1);
    ERRCHECK(result);

}

void get_spectrum(float* spectrum, size_t size)
{
    FMOD_RESULT result;
    result = FMOD_System_PlaySound(fmod_system, FMOD_CHANNEL_REUSE, sound, 0, &channel);
    ERRCHECK(result);

    result = FMOD_Channel_SetVolume(channel, 0);
    ERRCHECK(result);

    result = FMOD_Channel_GetSpectrum(channel, spectrum, size, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
    ERRCHECK(result);

    FMOD_System_Update(fmod_system);
}

void deinit()
{
    FMOD_RESULT result;
    result = FMOD_Sound_Release(sound);
    ERRCHECK(result);

    result = FMOD_System_Release(fmod_system);
    ERRCHECK(result);
}



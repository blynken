from __future__ import with_statement
from contextlib import contextmanager
from functools import partial
import socket

def parse_command(line):
    command, _, doc = line.partition(':')
    command = command.split(' ')
    command, args = command[0], [i[1:-1] for i in command[1:]]
    yield (command, args, doc)

@contextmanager
def connection(host, name, *args):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(host)
        sock.sendall(name + ' ' + ' '.join(str(i) for i in args) + '\r\n')
        yield sock
    finally:
        sock.close()


# TODO Error handling
class BaseContext(object):
    def __init__(self, host):
        self.connection = partial(connection, host)

        def command(func, name=None, cmd_args=None, doc=None):
            def decorate(*args, **kwargs):
                if not kwargs:
                    my_args = args[:len(cmd_args)]
                    # args = args[len(cmd_args):]
                else:
                    # .pop(i) statt [i]
                    my_args = [kwargs[i] for i in cmd_args]
                print my_args
                with self.connection(func.func_name, *my_args) as sock:
                    return func(sock, *args, **kwargs)
            decorate.func_name = func.func_name if not name else name
            decorate.func_doc = func.func_doc if not doc else doc
            return decorate

        self.command = command

        with self.connection('commands') as socket:
            for line in socket.makefile().readlines():
                for cmd, args, doc in parse_command(line):
                    func = None if not hasattr(self, cmd) else getattr(self, cmd)
                    if not hasattr(func, '__call__'):
                        func = lambda sock: sock.makefile().read()
                    setattr(self, cmd, command(func, cmd, args, doc))


class Context(BaseContext):
    def control(self, sock):
        pass

    def display(self, sock, x, y):
        while sock:
            print sock.recv(x * y)

    def add_movie(self, sock, name, data):
        if hasattr(data, 'read'):
            data = data.read()
        sock.sendall(data)


